# Slave-only docker setup 

This folder contains a Dockerfile, together with a few supporting files, for running a icecast intance in slave-only mode. The idea being able to host a larger set of slaves that just follow a master.

When running, the icecast instance in the container will start up a slave node which polls a master. The master server can be defined by environment variables.

## Env variables

Currently, 2 environment variables are supported: 

- `MASTER_SERVER` - The master's domain. 
- `MASTER_SERVER_PORT` - The master's port.

## How to build

From this directory, run docker build like this: 

```
docker build -f Dockerfile ..
```

This will build the image locally. In order to tag and upload to a repository, additional flags are needed. Without these flags, a hash is outputed as the image's id.

## Running

The docker image can be run using the docker run command:

```
docker run  -e MASTER_SERVER=localhost -e MASTER_SERVER_PORT=8000 -p 127.0.0.1:8001:8000 <img-id>
```

Be sure to replace `<img-id>` with the image id output above, and the localhost/8000 values with actual values.
