#! /usr/bin/env sh

set -e

sed -i "s/\$MASTER_SERVER_PORT/${MASTER_SERVER_PORT}/" /usr/share/icecast/icecast.xml
sed -i "s/\$MASTER_SERVER/${MASTER_SERVER}/" /usr/share/icecast/icecast.xml

icecast -c /usr/share/icecast/icecast.xml
